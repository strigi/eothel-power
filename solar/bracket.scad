
PANEL = [60.5, 125, 2.5];
TOLERANCE = 0.5;

EDGE = 2;
HEIGHT = 2 * PANEL[2];


POCKET = PANEL + [TOLERANCE, TOLERANCE, 0];
LOWER_POCKET = [
    POCKET[0] -2 * EDGE,
    POCKET[1] -2 * EDGE,
    20
];
FRAME = [
    POCKET[0] + 2 * EDGE,
    POCKET[1] + 2 * EDGE,
    10];


difference() {
    cube(FRAME);
    translate([EDGE, EDGE, FRAME[2] - POCKET[2]]) cube(POCKET);
    translate([2 * EDGE, 2 * EDGE]) cube(LOWER_POCKET);
}

ZWALUW_HEIGHT = 2;
#rotate(90, [0, 1, 0])
    translate([])
        linear_extrude(10)
            offset(delta=0)
                zwaluw(2, 5, ZWALUW_HEIGHT);

module zwaluw(top = 3, bottom = 5, height = 2) {    
    polygon([
        [-bottom / 2, 0],
        [- top / 2, height],
        [top / 2, height],
        [bottom / 2, 0],
    ]);
}


//linear_extrude(height = 10, center = true, convexity = 10, twist = 0)
//translate([2, 0, 0])

#include <Arduino.h>
#include <SD.h>
#include <RTClib.h>

class Logger {
    private:
        RTC_DS1307 rtc;

    public: 
        Logger() {
            Serial.begin(9600);            
            rtc.begin();
            //rtc.adjust(DateTime(__DATE__, __TIME__)); // Set RTC to date + time of compilation            
        }

        void info(String message) {
            log("INFO", message);
        }

/*        void info(float value) {
            log("INFO", String(value));
        }
*/
        void debug(String message) {
            log("DEBUG", message);
        }

        void error(String message) {
            log("ERROR", message);
        }

    protected:
        virtual void writeLine(String line) {
            Serial.println(line);
        }
    
    private:
        void log(String level, String message) {
            writeLine(timestamp() + " [" + level + "]\t" + message);
        }

        String leadingZero(byte value) {
            return (value < 10 ? String("0") : String("")) + value;
        }

        String timestamp() {
            DateTime now = rtc.now();

            String month = leadingZero(now.month());
            String day = leadingZero(now.day());
            String year = String(now.year());
            String hour = leadingZero(now.hour());
            String minute = leadingZero(now.minute());
            String second = leadingZero(now.second());
        
            String date = year + '-' + month + '-' + day;
            String time = hour + ':' + minute + ':' + second;

            return date + 'T' + time;
        }
};

class PersistentLogger : public Logger {
    private:
        SDClass sd;

    public:
        PersistentLogger() {
            info("Initializing SD card...");
            while(!sd.begin()) {
                error("Failed to initialize SD card. Retrying... (is there a card inserted?)");
            }
            debug("SD Card initialized!");
        }

    protected:
        void writeLine(String line) override {
            Logger::writeLine(line);

            File file = sd.open("log.txt", FILE_WRITE);
            file.println(line);
            file.close();
        }
};

float sense(float maximumVoltage) {
    float value = analogRead(0); // 0..1023 (10 bit ADC)
    return maximumVoltage * value / 1023.0;
}

PersistentLogger *logger = NULL;

void setup() {    
    pinMode(9, OUTPUT);

    logger = new PersistentLogger();

    logger->info("Welcome");
    logger->info("-------");
}

void loop() {
    // Measure open circuit voltage (Thevenin voltage)
    digitalWrite(9, LOW);
    float vo = sense(20);
    
    // Measure closed circuit voltage
    digitalWrite(9, HIGH);  // Switch on MOSFET to close circuit
    delay(1000);             // Let the current "flow" for a while (shorter than 100ms causes bad results)
    float vc = sense(20);
    digitalWrite(9, LOW);

    logger->info(String("Vo: ") + vo + "V, " + "Vc: " + vc + "V");
    
    // Wait one minute
    delay(60 * 1000L);
}
